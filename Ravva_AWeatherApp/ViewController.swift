//
//  ViewController.swift
//  Ravva_AWeatherApp
//
//  Created by Ravva,Shanmukha Manikantha Surya Vamsi on 3/15/17.
//  Copyright © 2017 Ravva,Shanmukha Manikantha Surya Vamsi. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var responseView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func getWeather(_ sender: Any) {
        
        let url = "http://api.openweathermap.org/data/2.5/weather?q="+city.text!+"&appid=2a3b119b50f51ab74393e58777a73dc6"
        let session:URLSession = URLSession.shared
        session.dataTask(with: URL(string: url)!, completionHandler: processResults).resume()
    }
    
    func processResults(data:Data?,response:URLResponse?,error:Error?)->Void {
        
        var weatherReport:Dictionary<String, Any>!
        
        print("DATA is :")
        print(data as Any)
        print("RESPONSE is :")
        print(response as Any)
        print("ERROR is :")
        print(error as Any)
        
        if error != nil {print(error as Any); return}
        
        do {
            weatherReport = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! Dictionary<String, Any>
            print(weatherReport)
            
            let weatherData = weatherReport["main"] as! Dictionary<String, Any>
            let skyData = weatherReport["weather"] as! Array<Dictionary<String,Any>>
            let wind = weatherReport["wind"] as! Dictionary<String, Any>
            
            //let sky = weatherData[0]
            
            //print("sky", (weatherReport["weather"])["description"])
            
            
            // Display the data in the text area
            
            DispatchQueue.main.async {
                //closure that will execute on main thread
                //Anything that changes the view must be on the main thread
                self.responseView.text = ""
                self.responseView.text.append( "Sky is \(skyData[0]["description"]!) \n")
                self.responseView.text.append( "Current temp: \(round(100 * (weatherData["temp"]! as! Double - 273)) / 100) C \n")
                self.responseView.text.append( "Max temp: \(round(100 * (weatherData["temp_max"]! as! Double - 273)) / 100) C \n")
                self.responseView.text.append( "Min temp: \(round(100 * (weatherData["temp_min"]! as! Double - 273)) / 100) C \n")
                self.responseView.text.append( "Humidity: \(round(100 * (weatherData["humidity"]! as! Double)) / 100 ) % \n")
                self.responseView.text.append( "Windspeed: \(round(100 * (wind["speed"]! as! Double)) / 100 ) ")
                return //return from closure
            }
            
        }catch {
            print("Unable to get weather report")
        }
        
    }

}

